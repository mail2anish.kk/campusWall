var express = require('express'),

	bodyParser = require('body-parser'),
	fs = require('fs'),
	http = require('http'),
	https = require('https'),
	morgan = require('morgan'),
	path = require('path'),
	crypto = require('crypto'),
	passport = require('passport');

// Main express App
var app = express();

/// Middleware goes here
app.use(require('cookie-parser')());
app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(bodyParser.json());



var mainRouter = express.Router();
app.use("/campuspay", mainRouter, function(req, res) {
	res.status(403).send("Bad request");
});


// Launching transactional roll back Fawn 

process.on('uncaughtException', function(err) {
	console.log('**************************');
	console.log('* [process.on(uncaughtException)]: err:', err);
	console.log('**************************');
	process.exit(1);
});

app.set('port', process.env.PORT || 3030);
http.createServer(app).listen(app.get('port'), function() {
	console.log('Express server listening on port ' + app.get('port'));
	//jobs.launchJobs();
});


module.exports.app = app;