var mongoose = require('../config/mongoose').Mongoose;

var Schema = mongoose.Schema;

var Role = new Schema({
	roleId: {
		type: Number,
		unique: true
	},
	name: String,
	access: [String],

	created_on: {
		type: Date,
		default: Date.now
	}

});

module.exports = mongoose.model('Role', Role);