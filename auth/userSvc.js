var dbUtil = require('../config/dbUtil');

var tableName = "users";
module.exports.findOne = function(queryObj, options) {
	options = options || {};
	return new Promise((resolve, reject) => {
		dbUtil.getConn().then((db) => {
			var coll = db.collection(tableName);
			coll.findOne(queryObj, options, (err, user) => {
				if (err) {
					reject(err);
				} else {
					resolve(user);
				}
			})
		});
	});
};

